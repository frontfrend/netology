import { People } from "../components/PeopleList/types";

const mock = {
  people: {
    "0": {
      id: "0",
      fname: "Mike",
      lname: "Smith",
      age: 24,
    },
    "1": {
      id: "1",
      fname: "Jack",
      lname: "Daniels",
      age: 31,
    },
    "2": {
      id: "2",
      fname: "Duke",
      lname: "Nukem",
      age: 31,
    },
    "3": {
      id: "3",
      fname: "Sam",
      lname: "Serious",
      age: 31,
    }
  },
}
export const fetchPeople = () =>
  new Promise<{ people: People }>((success) => {
    setTimeout(() => {
      success(mock);
    }, (Math.random() * 1 + 2) * 1000);
  });

const peopleApi = {
  fetchPeople,
};

export default peopleApi;
