import peopleApi from "./people";

const api = {
  people: peopleApi
}

export default api;