import React from "react"; //eslint-disable-line
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { AppContainer } from "react-hot-loader";
import App from "./App";
import store from "./redux/store";

const render = (Component: React.SFC) => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <Component />
      </Provider>
    </AppContainer>,
    document.getElementById("root")
  );
};

render(App);

if ((module as any).hot ) {
  (module as any).hot.accept("./App", () => {
    render(App);
    render(require("./App").default);
  });
}
