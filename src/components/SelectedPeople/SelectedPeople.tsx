import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import { Text, Pane, Heading } from "evergreen-ui";

const SelectedPeople = () => {
  const selectedNames = useSelector<RootState, string[]>((state) => {
    return Object.keys(state.people.checked).reduce((acc, id) => {
      if (state.people.checked[id]) {
        return [...acc, state.people.data[id].fname];
      }
      return acc;
    }, [] as string[]);
  });
  
  if (!selectedNames.length) {
    return null;
  }

  return (
    <Pane marginTop={20}>
      <Heading is="h3">
        Selected {selectedNames.length > 1 ? "people" : "person"}:
      </Heading>
      <Text size={300}>{selectedNames.toString()}</Text>
    </Pane>
  );
};

export default SelectedPeople;
