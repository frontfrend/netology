import React, { useMemo, useCallback, memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import { CheckedType } from "../PeopleList/redux/reducer";
import { Checkbox } from "evergreen-ui";
import people from "../PeopleList/redux/actions";

const HeaderCheckbox = memo(() => {
  const dispatch = useDispatch();

  const checked = useSelector<RootState, CheckedType>(
    (state) => state.people.checked
  );

  const isAllSelected = useMemo(() => {
    return Object.values(checked).every((checkedItem) => checkedItem);
  }, [checked]);

  const onToggle = useCallback(() => {
    if (isAllSelected) {
      dispatch(people.untoggleAll());
    } else {
      dispatch(people.toggleAll());
    }
  }, [isAllSelected, dispatch]);

  return (
    <Checkbox
      label={isAllSelected ? "Deselect All" : "Select all"}
      checked={isAllSelected}
      onChange={onToggle}
    />
  );
});

export default HeaderCheckbox;