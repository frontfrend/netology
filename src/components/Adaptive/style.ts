import styled from "styled-components";

export const Picture = styled.picture`
  width: 100%;
  display: flex;
  justify-content: center;
`;
