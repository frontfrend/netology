import React from "react";
import pc from "./resources/pc.gif";
import tablet from "./resources/tablet.webp";
import phone from "./resources/phone.gif";
import { Picture } from "./style";
import { Pane, Heading } from "evergreen-ui";

const Adaptive = () => {
  return (
    <Pane marginTop={20}>
      <Heading is="h3" marginBottom={20}>
        Adaptive animation:
      </Heading>
      <Picture>
        <source srcSet={phone} media="(max-width: 767px)" />
        <source srcSet={tablet} media="(max-width: 1023px)" />
        <source srcSet={pc} media="(min-width: 1024px)" />
        <img src={pc} alt="Resolution dependent animation" />
      </Picture>
    </Pane>
  );
};

export default Adaptive;
