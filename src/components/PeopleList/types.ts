export type PersonType = {
  id: string;
  fname: string;
  lname: string;
  age: number;
};


export type People = {[key: string]: PersonType}