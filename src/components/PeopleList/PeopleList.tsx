import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import people from "./redux/actions";
import { RootState } from "../../redux/store";
import { Table, Spinner, Pane } from "evergreen-ui";
import HeaderCheckbox from "../RowCheckbox/HeaderCheckbox";
import Row from "./Row";

const PeopleList = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(people.load());
  }, [dispatch]);

  const loading = useSelector<RootState, boolean>(
    (state) => state.people.loading
  );
  const ids = useSelector<RootState, string[]>((state) => state.people.ids);

  if (loading) {
    return (
      <Pane
        display="flex"
        alignItems="center"
        justifyContent="center"
        height={400}
      >
        <Spinner />
      </Pane>
    );
  }

  return (
    <Table>
      <Table.Head>
        <Table.TextHeaderCell  flexBasis={120} flexShrink={0} flexGrow={0}>
          <HeaderCheckbox />
        </Table.TextHeaderCell>
        <Table.TextHeaderCell>First name</Table.TextHeaderCell>
        <Table.TextHeaderCell>Last name</Table.TextHeaderCell>
        <Table.TextHeaderCell>Age</Table.TextHeaderCell>
      </Table.Head>
      <Table.Body>
        {ids.map((id) => (
          <Row key={id} id={id} />
        ))}
      </Table.Body>
    </Table>
  );
};

export default PeopleList;
