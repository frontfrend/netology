import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import people from "./actions";
import { People } from "../types";

export type CheckedType = { [key: string]: boolean }

export type PeopleState = {
  loading: boolean;
  data: People;
  ids: string[];
  checked: CheckedType;
};



const peopleSlice = createSlice({
  name: "people",
  initialState: {
    loading: false,
    data: {} as People,
    ids: [] as string[],
    checked: {} as { [key: string]: boolean },
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(people.load.pending, (state) => {
        state.loading = true;
      })
      .addCase(people.load.rejected, (state) => {
        state.loading = false;
      })
      .addCase(
        people.load.fulfilled,
        (state, action: PayloadAction<{ people: People }>) => {
          state.loading = false;
          state.data = action.payload.people;
          state.ids = Object.keys(state.data);
          state.checked = Object.keys(state.data).reduce((acc, id) => {
            return { ...acc, [id]: false };
          }, {});
        }
      )
      .addCase(people.toggleById, (state, action) => {
        state.checked[action.payload.id] = !state.checked[action.payload.id];
      })
      .addCase(people.toggleAll, (state) => {
        state.checked = Object.keys(state.data).reduce((acc, id) => {
          return { ...acc, [id]: true };
        }, {});
      })
      .addCase(people.untoggleAll, (state) => {
        state.checked = Object.keys(state.data).reduce((acc, id) => {
          return { ...acc, [id]: false };
        }, {});
      });
  },
});

export default peopleSlice;
