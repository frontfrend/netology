import { createAsyncThunk, createAction } from "@reduxjs/toolkit";
import { People } from "../types";
import api from "../../../api";

const load = createAsyncThunk<{ people: People }>(
  "people/load",
  async (_, thunkApi) => {
    try {
      const people = await api.people.fetchPeople();
      return people;
    } catch (err) {
      return thunkApi.rejectWithValue(err);
    }
  }
);

const toggleAll = createAction("people/toggleAll");
const untoggleAll = createAction("people/untoggleAll");

const toggleById = createAction("people/toggleById", (id) => {
  return {
    payload: { id },
  };
});

const people = {
  load,
  toggleAll,
  untoggleAll,
  toggleById,
};

export default people;
