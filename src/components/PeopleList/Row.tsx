import React, { memo, useCallback } from "react";
import { Table, Checkbox } from "evergreen-ui";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/store";
import { PersonType } from "./types";
import people from "./redux/actions";
import { CustomRow } from "./style";

const Row = memo(({ id }: { id: string }) => {
  const dispatch = useDispatch();

  const person = useSelector<RootState, PersonType>(
    (state) => state.people.data[id]
  );

  const isChecked = useSelector<RootState, boolean>(
    (state) => state.people.checked[id]
  );

  const onToggle = useCallback(() => {
    dispatch(people.toggleById(id));
  }, [dispatch, id]);

  return (
    <CustomRow selected={isChecked}>
      <Table.Cell flexBasis={120} flexShrink={0} flexGrow={0}>
        <Checkbox
          checked={isChecked}
          label={isChecked ? "Deselect" : "Select"}
          onChange={onToggle}
        />
      </Table.Cell>
      <Table.TextCell>{person.fname}</Table.TextCell>
      <Table.TextCell>{person.lname}</Table.TextCell>
      <Table.TextCell isNumber>{person.age}</Table.TextCell>
    </CustomRow>
  );
});

export default Row;
