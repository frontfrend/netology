import styled, { keyframes } from "styled-components";
import { Table } from "evergreen-ui";

const backgroundAnimation = keyframes`
 0% { background-color: #FEE4E3 }
 20% { background-color: #EBEBEB }
 40% { background-color: #D9E3ED }
 60% { background-color: #869BAE }
 80% { background-color: #B1CDD0 }
 100% { background-color: #FEE4E3 }
`

export const CustomRow = styled(Table.Row)`
  animation-name: ${props => props.selected && backgroundAnimation};
  animation-duration: 8s;
  animation-iteration-count: infinite;
`