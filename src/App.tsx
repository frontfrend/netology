import React from "react";
import PeopleList from "./components/PeopleList/PeopleList";
import SelectedPeople from "./components/SelectedPeople/SelectedPeople";
import Adaptive from "./components/Adaptive/Adaptive";
import { Heading, Pane } from "evergreen-ui";
import styled from "styled-components";

export const Layout = styled.div`
  max-width: 600px;
  margin: 0 auto;
`

const App = () => {
  return (
    <Layout>
      <Pane
        marginY={40}
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Heading size={700} is="h1">
          Test App
        </Heading>
      </Pane>

      <PeopleList />
      <SelectedPeople />
      <Adaptive />
    </Layout>
  );
};

export default App;
